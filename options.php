<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;

global $APPLICATION;

$moduleID = !empty(["mid"]) ? $_REQUEST["mid"] : $_REQUEST["id"];

Loc::loadMessages(__FILE__);
Loader::includeModule($moduleID);

$options = new \MyCurrency\Options($moduleID);
$arOptions = $options->getOptions();

if (check_bitrix_sessid()) {
    if ($_POST["apply"]) {
        foreach ($arOptions as $option) {
            foreach ($option["OPTIONS"] as $arOption) {
                if (!is_array($arOption)) {
                    continue;
                }
                $options->setOption($arOption, $_POST[$arOption[0]]);
            }
        }
    } elseif ($_POST["default"]) {
        $options->setDefaultOptions();
    }
}

$tabControl = new CAdminTabControl("tabControl", $arOptions);
$tabControl->Begin();

?>

<form action="<?= $APPLICATION->GetCurPage() ?>?mid=<?= $moduleID ?>&lang=<?= LANG ?>" method="post">
    <?php
    foreach ($arOptions as $option) {
        if ($option["OPTIONS"]) {
            $tabControl->BeginNextTab();

            __AdmSettingsDrawList($moduleID, $option["OPTIONS"]);
        }
    }

    $tabControl->Buttons();
    ?>

    <input value="<?= Loc::getMessage("MYCURRENCY_OPTION_BUTTON_APPLY_TEXT") ?>"
           type="submit" name="apply" class="adm-btn-save"
    />
    <input value="<?= Loc::getMessage("MYCURRENCY_OPTION_BUTTON_DEFAULT_TEXT") ?>"
           type="submit" name="default"/>

    <?= bitrix_sessid_post(); ?>

</form>
