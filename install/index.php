<?php

use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Application;
use Bitrix\Main\IO\Directory;

Loc::loadMessages(__FILE__);

class mycurrency extends CModule
{
    public $MODULE_ID;
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_GROUP_RIGHTS = 'Y';

    public function __construct()
    {
        if (file_exists(__DIR__ . "/version.php")) {
            $arModuleVersion = array();

            include_once(__DIR__ . "/version.php");

            $this->MODULE_ID = str_replace("_", ".", get_class($this));
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
            $this->MODULE_NAME = Loc::getMessage("MYCURRENCY_NAME");
            $this->MODULE_DESCRIPTION = Loc::getMessage("MYCURRENCY_DESCRIPTION");
        }
    }

    public function InstallFiles()
    {
        CopyDirFiles(
            __DIR__ . "/components",
            Application::getDocumentRoot() . "/bitrix/components",
            true,
            true
        );
        CopyDirFiles(
            __DIR__ . "/admin",
            Application::getDocumentRoot() . "/bitrix/admin",
            true,
            true
        );
        return true;
    }

    public function UnInstallFiles()
    {
        DeleteDirFiles(
            __DIR__ . "/admin",
            Application::getDocumentRoot() . "/bitrix/admin"
        );
        Directory::deleteDirectory(Application::getDocumentRoot() . "/bitrix/components/mycurrency");
        return true;
    }

    public function InstallDB()
    {
        global $DB, $DBType, $APPLICATION;

        if (!$DB->Query("SELECT 'x' FROM b_currency", true)) {
            $errors = $DB->RunSQLBatch(dirname(__DIR__) . "/{$DBType}/install.sql");
            if (!empty($errors)) {
                $APPLICATION->ThrowException(implode("", $errors));
                return false;
            }
        }
        return true;
    }

    public function UnInstallDB($arParams = array())
    {
        global $APPLICATION, $DB, $DBType;

        if (array_key_exists("savedata", $arParams) && $arParams["savedata"] !== "Y") {
            $errors = $DB->RunSQLBatch(dirname(__DIR__) . "/{$DBType}/uninstall.sql");
            if (!empty($errors)) {
                $APPLICATION->ThrowException(implode("", $errors));
                return false;
            }
        }
        return true;
    }

    public function DoInstall()
    {
        global $APPLICATION;

        $this->InstallFiles();
        $this->InstallDB();

        ModuleManager::registerModule($this->MODULE_ID);

        $this->addAgent();
        $this->setInitialOptions();

        $APPLICATION->IncludeAdminFile(
            Loc::getMessage("MYCURRENCY_INSTALL_TITLE"),
            __DIR__ . "/step.php"
        );
    }

    public function DoUninstall()
    {
        global $APPLICATION, $step;

        $step = (int)$step;

        if ($step < 2) {
            $APPLICATION->IncludeAdminFile(
                Loc::getMessage("MYCURRENCY_UNINSTALL_TITLE"),
                __DIR__ . "/unstep1.php"
            );
        } elseif ($step == 2) {
            $this->UnInstallFiles();
            $this->UnInstallDB(array("savedata" => $_REQUEST["savedata"]));

            $this->deleteAgent();
            $this->deleteOptions(array("saveoptions" => $_REQUEST["saveoptions"]));

            ModuleManager::unRegisterModule($this->MODULE_ID);

            $APPLICATION->IncludeAdminFile(
                Loc::getMessage("MYCURRENCY_UNINSTALL_TITLE"),
                __DIR__ . "/unstep2.php"
            );
        }
    }

    private function setInitialOptions()
    {
        \Bitrix\Main\Loader::includeModule($this->MODULE_ID);

        $options = new \MyCurrency\Options($this->MODULE_ID);
        if ($options->checkOptionsNotExist()) {
            $options->setDefaultOptions();
        }
    }

    private function deleteOptions($arParams)
    {
        if (array_key_exists("saveoptions", $arParams) && $arParams["saveoptions"] !== "Y") {
            \Bitrix\Main\Loader::includeModule($this->MODULE_ID);

            $options = new \MyCurrency\Options($this->MODULE_ID);
            $options->deleteOptions();
        }
    }

    private function addAgent()
    {
        CAgent::AddAgent(
            "mycurrency::agentUpdateCurrency();",
            $this->MODULE_ID,
            "N",
            86400
        );
    }

    private function deleteAgent()
    {
        CAgent::RemoveAgent(
            "mycurrency::agentUpdateCurrency();",
            $this->MODULE_ID
        );
    }

    /**
     * Агент, обновляющий курс валют каждые 24 часа
     * @return string
     */
    public function agentUpdateCurrency()
    {
        try {
            $cbr = new \MyCurrency\CBR();
            $currencies = $cbr->getXML();
        } catch (SoapFault $fault) {
            return "mycurrency::agentUpdateCurrency();";
        }

        $codes = MyCurrency\MyCurrencyTable::getList(array(
            'select' => array('CHCODE', 'ID')
        ));

        while ($row = $codes->fetch()) {
            $currency = $currencies->getRateByCurrencyCode($row['CHCODE']);
            \MyCurrency\Rate::addRate($row['ID'], $currency);
        }

        return "mycurrency::agentUpdateCurrency();";
    }
}
