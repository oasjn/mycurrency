<?php

use \Bitrix\Main\Localization\Loc;

if (!check_bitrix_sessid()) {
    return;
}

Loc::loadMessages(__FILE__);

global $APPLICATION;

?>

<form action="<?php echo $APPLICATION->GetCurPage(); ?>">
    <?php echo bitrix_sessid_post(); ?>
    <p><?= Loc::getMessage("MYCURRENCY_DELETE_TABLES_TITLE") ?></p>
    <input type="hidden" name="lang" value="<?php echo LANGUAGE_ID; ?>">
    <input type="hidden" name="id" value="mycurrency">
    <input type="hidden" name="uninstall" value="Y">
    <input type="hidden" name="step" value="2">
    <?php CAdminMessage::ShowMessage(GetMessage('MOD_UNINST_WARN')); ?>
    <p><?php echo GetMessage('MOD_UNINST_SAVE') ?></p>
    <p>
        <input type="checkbox" name="savedata" id="savedata" value="Y" checked>
        <label for="savedata"><?= Loc::getMessage("MYCURRENCY_DELETE_TABLES_LABEL_SAVE") ?></label>
    </p>
    <p>
        <input type="checkbox" name="saveoptions" id="saveoptions" value="Y" checked>
        <label for="saveoptions"><?= Loc::getMessage("MYCURRENCY_DELETE_OPTIONS_LABEL_SAVE") ?></label>
    </p>
    <input type="submit" name="inst" value="<?= Loc::getMessage("MYCURRENCY_DELETE_TABLES_BUTTON_TEXT_CONTINUE") ?>">
</form>
