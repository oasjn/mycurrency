<?php
if (!check_bitrix_sessid()) {
    return;
}

CAdminMessage::ShowNote('OK');
?>

<form action="<?php echo $APPLICATION->GetCurPage(); ?>">
<p>
    <input type="hidden" name="lang" value="<?php echo LANGUAGE_ID; ?>">
    <input type="submit" name="" value="<?php echo GetMessage('MOD_BACK'); ?>">
</p>
<form>