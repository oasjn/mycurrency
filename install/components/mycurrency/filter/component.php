<?php

use Bitrix\Main\Application;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}


if ($this->StartResultCache()) {
    $arResult['sTableID'] = $arParams['sTableID'];
    $arResult['adminFilter'] = $this->createFilter($arParams['sTableID']);

    $arCurrencyNames = array();
    try {
        $arCurrencyNames = $this->getCurrencyNames();
    } catch (\SoapFault $fault) {
        CAdminMessage::ShowMessage($fault->getMessage());
    }
    $arResult['arCurrencyNames'] = $arCurrencyNames;
    $this->IncludeComponentTemplate();
}
