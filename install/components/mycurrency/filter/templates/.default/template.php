<?

use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

Loc::loadMessages(__FILE__);

global $APPLICATION;

$adminFilter = $arResult['adminFilter'];
$arCurrencyNames = $arResult['arCurrencyNames'];
$sTableID = $arResult['sTableID'];

$form_name = "filter_mycurrency";
?>

<form method="get" action="<? $APPLICATION->GetCurPage(); ?>" name="<?= $form_name ?>">
    <? $adminFilter->Begin(); ?>
    <tr>
        <td><?= Loc::getMessage("MYCURRENCY_FILTER_FIELD_CODE") ?>:</td>
        <td>
            <select name="filter_chcode">
                <? if (empty($_REQUEST['filter_chcode'])): ?>
                    <option selected value="none">--</option>
                <? else: ?>
                    <option value="none">--</option>
                <? endif; ?>
                <? foreach ($arCurrencyNames as $key => $value): ?>
                    <? if ($_REQUEST['filter_chcode'] === $key): ?>
                        <option selected value=<?= $key ?>><?= $key ?></option>
                    <? else: ?>
                        <option value=<?= $key ?>><?= $key ?></option>
                    <? endif; ?>
                <? endforeach; ?>
            </select>
        </td>
    </tr>
    <tr>
        <td><?= Loc::getMessage("MYCURRENCY_FILTER_FIELD_RATE") ?>:</td>
        <td>
            <label><?= Loc::getMessage("MYCURRENCY_FILTER_FIELD_RATE_FROM") ?></label>
            <input name="filter_rate_from" type="text" value=<?= $_REQUEST['filter_rate_from'] ?>>
        </td>

    </tr>
    <tr>
        <td></td>
        <td>
            <label><?= Loc::getMessage("MYCURRENCY_FILTER_FIELD_RATE_TO") ?></label>
            <input name="filter_rate_to" type="text" value=<?= $_REQUEST['filter_rate_to'] ?>>
        </td>
    </tr>

    <?
    $adminFilter->Buttons(array(
        "table_id" => $sTableID,
        "url" => $APPLICATION->GetCurPage(),
        "form" => $form_name
    ));
    $adminFilter->End();
    ?>
</form><br>