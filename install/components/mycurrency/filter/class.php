<?php

namespace MyCurrency;

use \Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

Loc::loadMessages(__FILE__);

class FilterCurrency extends \CBitrixComponent
{
    /**
     * @return array
     */
    public function getCurrencyNames()
    {
        $cbr = (new \MyCurrency\CBR());
        $xml = $cbr->getXML();
        return $xml->getCurrencyNames();
    }

    /**
     * @param string $tableID
     * @return \CAdminFilter
     */
    public function createFilter($tableID)
    {
        $adminFilter = new \CAdminFilter($tableID . "_filter", array(
            Loc::getMessage("MYCURRENCY_FILTER_FIELD_CODE"),
            Loc::getMessage("MYCURRENCY_FILTER_FIELD_RATE_FROM"),
            Loc::getMessage("MYCURRENCY_FILTER_FIELD_RATE_TO"),
        ));
        return $adminFilter;
    }
}
