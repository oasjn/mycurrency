<?php
use \Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
    "NAME" => Loc::getMessage("MYCURRENCY_FILTER_NAME"),
    "DESCRIPTION" => Loc::getMessage("MYCURRENCY_FILTER_DESCRIPTION"),
    "PATH" => array(
        "ID" => "content",
		"CHILD" => array(
			"ID" => "mycurrency.filter",
			"NAME" => Loc::getMessage("MYCURRENCY_FILTER_NAME")
		)
    ),
);


