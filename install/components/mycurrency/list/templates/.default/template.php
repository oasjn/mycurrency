<?

use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

Loc::loadMessages(__FILE__);

global $APPLICATION;
?>

<form method="post" action="<? $APPLICATION->GetCurPage() ?>" name="form_load_rate">
    <a href="add_mycurrency.php">
        <input type="button" value="<?=Loc::getMessage("MYCURRENCY_LIST_BUTTON_ADD") ?>">
    </a>
</form>
