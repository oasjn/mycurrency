<?php
use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

Loc::loadMessages(__FILE__);

global $APPLICATION;

if ($this->StartResultCache()) {
    $this->IncludeComponentTemplate();
}

$APPLICATION->SetTitle(Loc::getMessage("MYCURRENCY_LIST_TITLE"));
