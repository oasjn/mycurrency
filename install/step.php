<?php

use \Bitrix\Main\Localization\Loc;

if (!check_bitrix_sessid()) {
    return;
}

Loc::loadMessages(__FILE__);

global $APPLICATION;

echo CAdminMessage::ShowNote(Loc::getMessage("MYCURRENCY_MODULE_INSTALLED"));
?>

<form action="<?php echo $APPLICATION->GetCurPage(); ?>">
    <p>
        <input type="hidden" name="lang" value="<?php echo LANGUAGE_ID; ?>">
        <input type="submit" name="" value="<?= Loc::getMessage("MYCURRENCY_BUTTON_BACK_TEXT"); ?>">
    </p>
    <form>

