create table if not exists my_currency
(
  ID int not null auto_increment,
  CODE smallint(3) not null,
  CHCODE char(3) not null,
  NAME varchar(70) not null,
  primary key (ID),
  unique index code_ind (CODE)
);

create table if not exists my_currency_rate
(
  ID int not null auto_increment,
  ID_CURRENCY int not null,
  DATE datetime not null,
  RATE float not null,
  NOMINAL int not null,
  primary key (ID),
  index valute_ind (ID_CURRENCY),
  unique index rate_int (ID_CURRENCY, DATE),
  FOREIGN KEY (ID_CURRENCY)
    REFERENCES my_currency(ID)
    ON DELETE CASCADE
);
