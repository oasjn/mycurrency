<?php

use Bitrix\Main\Localization\Loc;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

Loc::loadMessages(__FILE__);
\Bitrix\Main\Loader::includeModule("mycurrency");

global $APPLICATION;

$POST_RIGHT = $APPLICATION->GetGroupRight("mycurrency");
if ($POST_RIGHT == "D") {
    $APPLICATION->AuthForm(Loc::getMessage("MYCURRENCY_ACCESS_DENIED"));
}

$arMsg = array();
try {
    $cbr = new \MyCurrency\CBR();
    $xml = $cbr->getXML();

    $arCurrencyNames = $xml->getCurrencyNames();

    if (!empty($_POST["chcode_currency"])) {
        $currency = $xml->getRateByCurrencyCode($_POST["chcode_currency"]);

        $errors = \MyCurrency\Currency::addCurrency($currency);

        $arMsg = array_merge($arMsg, $errors);
        if (empty($errors)) {
            $added = true;
        }
    }
} catch (SoapFault $fault) {
    $arMsg = array_merge($arMsg, array($fault->getMessage()));
}


require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

if (!empty($arMsg)) {
    foreach ($arMsg as $message) {
        echo CAdminMessage::ShowMessage($message);
    }
}
if ($added) {
    echo CAdminMessage::ShowNote(Loc::getMessage("MYCURRENCY_ADD_CURRENCY_SUCCESS"));
}

?>

    <form method="post" action="<?php $APPLICATION->GetCurPage() ?>" name="add_mycurrency">
        <p><?= Loc::getMessage("MYCURRENCY_CHOOSE_CURRENCY") ?></p>
        <select name="chcode_currency">
            <?php foreach ($arCurrencyNames as $key => $value): ?>
                <?php if ($key === $_POST["chcode_currency"]): ?>
                    <option selected value=<?= $key ?>><?= $value ?></option>
                <?php else: ?>
                    <option value=<?= $key ?>><?= $value ?></option>
                <?php endif; ?>
            <?php endforeach; ?>
        </select>
        <input type="submit" name="addCurrency" value=<?= Loc::getMessage("MYCURRENCY_BUTTON_ADD_TEXT") ?>>
        <a href="mycurrency.php"><input type="button" name="backtolist"
                                        value=<?= Loc::getMessage("MYCURRENCY_BUTTON_BACK_TEXT") ?>></a>
    </form>

<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
