<?php

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

global $APPLICATION;

if ($APPLICATION->GetGroupRight('mycurrency') == 'D') {
    return false;
}

return array(
    "parent_menu" => "global_menu_settings",
    "sort" => 300,
    "url" => "mycurrency.php",
    "more_url" => array(
        "add_mycurrency.php",
    ),
    "text" => Loc::GetMessage("MYCURRENCY_MENU_TEXT"),
    "title" => Loc::GetMessage("MYCURRENCY_MENU_TITLE"),
    "items_id" => "menu_mycurrency",
);
