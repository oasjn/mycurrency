<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Entity\ExpressionField;
use Bitrix\Main\Config\Option;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once(dirname(__DIR__) . "/include.php");

Loc::loadMessages(__FILE__);
Bitrix\Main\Loader::includeModule("mycurrency");

global $APPLICATION, $DB;

$POST_RIGHT = $APPLICATION->GetGroupRight("mycurrency");
if ($POST_RIGHT == "D") {
    $APPLICATION->AuthForm(Loc::getMessage("MYCURRENCY_ACCESS_DENIED"));
}

$currencyList = new \MyCurrency\CurrencyList();
$addActions = false;
if ($POST_RIGHT === "W") {
    $currencyList->handleActions($_REQUEST['action_target'], $_REQUEST['action_button']);
    $addActions = true;
}

$currencyList->formListResult($_REQUEST, $addActions);

$adminList = $currencyList->getAdminList();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

$APPLICATION->IncludeComponent(
    "mycurrency:filter",
    ".default",
    array(
        "sTableID" => $currencyList::TABLE_ID,
        "COMPONENT_TEMPLATE" => ".default",
        "SEF_MODE" => "Y",
        "SET_TITLE" => "Y",
        "CACHE_TYPE" => "A",
    ),
    false
);

$adminList->DisplayList();

$APPLICATION->IncludeComponent(
    "mycurrency:list",
    ".default",
    array(
        "COMPONENT_TEMPLATE" => ".default",
        "SEF_MODE" => "Y",
        "SET_TITLE" => "Y",
        "CACHE_TYPE" => "A",
    ),
    false
);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
