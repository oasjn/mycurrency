<?php
$MESS["MYCURRENCY_NAME"] = "mycurrency - курсы валют";
$MESS["MYCURRENCY_DESCRIPTION"] = "Модуль, отображающий курсы валют";

$MESS["MYCURRENCY_INSTALL_TITLE"] = "Установка модуля mycurrency";
$MESS["MYCURRENCY_UNINSTALL_TITLE"] = "Удаление модуля mycurrency";
