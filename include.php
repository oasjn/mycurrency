<?php
use Bitrix\Main\Loader;

Loader::registerAutoLoadClasses(
    'mycurrency',
    array(
        'mycurrency' => 'install/index.php',
        '\MyCurrency\MyCurrencyTable' => 'lib/mycurrency.php',
        '\MyCurrency\MyCurrencyRateTable' => 'lib/mycurrency_rate.php',
        '\MyCurrency\CBR' => 'lib/cbr.php',
        '\MyCurrency\XML' => 'lib/xml.php',
        '\MyCurrency\Currency' => 'lib/currency.php',
        '\MyCurrency\Rate' => 'lib/rate.php',
        '\MyCurrency\Options' => 'lib/options.php',
        '\MyCurrency\CurrencyList' => 'lib/currency_list.php',
        '\MyCurrency\CurrencyRate' => 'lib/currency_rate.php',
    )
);
