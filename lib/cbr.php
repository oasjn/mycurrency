<?php

namespace MyCurrency;

class CBR
{
    // WSDL службы Центробанка
    const WSDL = "http://www.cbr.ru/DailyInfoWebServ/DailyInfo.asmx?WSDL";
    // Экземпляр класса SoapClient
    protected $soap;

    public function __construct()
    {
        $this->soap = new \SoapClient(CBR::WSDL);
    }


    /**
     * @return string
     */
    private function formatDate()
    {
        $date = new \DateTime();
        return $date->format('Y-m-d') . 'T' . $date->format('H:i:s');
    }

    /**
     * Получение XML курсов на текущую дату
     * @return XML
     */
    public function getXML()
    {
        $result = $this->soap->GetCursOnDateXML(array('On_date' => $this->formatDate()));
        $xml = new \SimpleXMLElement($result->GetCursOnDateXMLResult->any);
        return new XML($xml);
    }
}
