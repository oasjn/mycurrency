<?php

namespace MyCurrency;

class XML
{
    // результат запроса к cbr
    protected $xml;
    // дата полученных курсов
    protected $date;

    public function __construct($xml)
    {
        $this->xml = $xml;
        $this->date = $this->getDate();
    }

    /**
     * @return array
     */
    public function getCurrencyNames()
    {
        $arCurrencies = json_decode(json_encode($this->xml->xpath("/ValuteData/ValuteCursOnDate")));

        $resultName = array();
        foreach ($arCurrencies as $currency) {
            $resultName[$currency->VchCode] = $currency->Vname;
        }
        return $resultName;
    }

    /**
     * @return \DateTime
     */
    protected function getDate()
    {
        $date = $this->xml->attributes()->OnDate;
        return new \DateTime($date[0]);
    }

    /**
     * @param string $currencyChCode - символьный код валюты
     * @return object
     */
    public function getRateByCurrencyCode($currencyChCode)
    {
        $arCurrencyRate = $this->xml->xpath("/ValuteData/ValuteCursOnDate[VchCode='{$currencyChCode}']");
        $arCurrencyRate[0]->addChild('Vdate', $this->date->format('d.m.Y'));
        return json_decode(json_encode($arCurrencyRate[0]));
    }
}
