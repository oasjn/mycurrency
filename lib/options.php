<?php

namespace MyCurrency;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;

Loc::loadMessages(__FILE__);


class Options
{
    private $moduleID;
    // настройки модуля
    private $options;

    public function __construct($moduleID)
    {
        $this->moduleID = $moduleID;
        $this->options = array(
            array(
                "TAB" => Loc::getMessage("MYCURRENCY_OPTION_TITLE_TAB_MAIN"),
                "TITLE" => Loc::getMessage("MYCURRENCY_OPTION_TITLE_TAB_MAIN"),
                "OPTIONS" => array(
                    Loc::getMessage("MYCURRENCY_OPTION_SWITCH_TITLE"),
                    array(
                        "switch_on",
                        Loc::getMessage("MYCURRENCY_OPTION_SWITCH_TEXT"),
                        "Y",
                        array("checkbox")
                    ),
                    Loc::getMessage("MYCURRENCY_OPTION_FIELD_TITLE"),
                    array(
                        "ID",
                        Loc::getMessage("MYCURRENCY_OPTION_FIELD_ID_TEXT"),
                        "N",
                        array("checkbox")
                    ),
                    array(
                        "CODE",
                        Loc::getMessage("MYCURRENCY_OPTION_FIELD_CODE_TEXT"),
                        "Y",
                        array("checkbox")
                    ),
                    array(
                        "CHCODE",
                        Loc::getMessage("MYCURRENCY_OPTION_FIELD_CHCODE_TEXT"),
                        "Y",
                        array("checkbox")
                    ),
                    array(
                        "NAME",
                        Loc::getMessage("MYCURRENCY_OPTION_FIELD_NAME_TEXT"),
                        "Y",
                        array("checkbox")
                    ),
                    array(
                        "NOMINAL",
                        Loc::getMessage("MYCURRENCY_OPTION_FIELD_NOMINAL_TEXT"),
                        "Y",
                        array("checkbox")
                    ),
                    array(
                        "RATE",
                        Loc::getMessage("MYCURRENCY_OPTION_FIELD_RATE_TEXT"),
                        "Y",
                        array("checkbox")
                    ),
                    array(
                        "DATE",
                        Loc::getMessage("MYCURRENCY_OPTION_FIELD_DATE_TEXT"),
                        "Y",
                        array("checkbox")
                    ),
                )
            )
        );
    }

    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @return bool
     */
    public function checkOptionsNotExist()
    {
        $arOptions = Option::getForModule($this->moduleID);
        if (!empty($arOptions)) {
            return false;
        }
        return true;
    }

    public function deleteOptions()
    {
        foreach ($this->options as $option) {
            foreach ($option["OPTIONS"] as $arOption) {
                if (!is_array($arOption)) {
                    continue;
                }
                Option::delete($this->moduleID, $arOption[0]);
            }
        }
    }

    /**
     * @param array $arOption - массив настроек
     * @param string $optionValue
     */
    public function setOption($arOption, $optionValue)
    {
        if ($optionValue == "") {
            $optionValue = "N";
        }

        Option::set(
            $this->moduleID,
            $arOption[0],
            $optionValue
        );
    }

    public function setDefaultOptions()
    {
        foreach ($this->options as $option) {
            foreach ($option["OPTIONS"] as $arOption) {
                if (!is_array($arOption)) {
                    continue;
                }
                Option::set($this->moduleID, $arOption[0], $arOption[2]);
            }
        }
    }
}
