<?php

namespace MyCurrency;

class CurrencyRate
{
    /**
     * @return array
     */
    public static function getIDAllCurrencies()
    {
        $currencies = Currency::getAll();

        $IDs = array();
        foreach ($currencies as $currency) {
            $IDs[] = $currency['ID'];
        }
        return $IDs;
    }

    /**
     * @return array
     */
    public static function formFilter($request)
    {
        $filter = array(
            'currency' => array(),
            'rate' => array(
                'RATE' => array(),
            ),
        );

        if (!empty($request['set_filter'])) {
            if (!empty($request['filter_chcode']) && $request['filter_chcode'] !== 'none') {
                $filter['currency']["=CHCODE"] = $request['filter_chcode'];
            }
            if (!empty($request['filter_rate_to'])) {
                array_push($filter['rate']['RATE'], array(
                    'filter' => '<=',
                    'value' => $request['filter_rate_to']
                ));
            }
            if (!empty($request['filter_rate_from'])) {
                array_push($filter['rate']['RATE'], array(
                    'filter' => '>=',
                    'value' => $request['filter_rate_from']
                ));
            }
        }
        return $filter;
    }

    /**
     * @return array
     */
    public static function formOrder($request)
    {
        $order = array(
            'rate' => array(),
            'currency' => array(),
        );
        if (!empty($request['order']) && !empty($request['by'])) {
            switch ($request['by']) {
                case 'ID':
                case 'CODE':
                case 'CHCODE':
                case 'NAME':
                    $order['currency'][$request['by']] = $request['order'];
                    break;
                case 'NOMINAL':
                case 'RATE':
                    $order['rate']['by'] = $request['by'];
                    $order['rate']['order'] = $request['order'];
                    break;
            }
        }

        return $order;
    }

    /**
     * @param array $filter
     * @param array $order
     * @return array
     */
    public static function getCurrenciesWithLastRate($filter, $order)
    {
        $currencies = MyCurrencyTable::getList(array(
            'select' => array('*'),
            'filter' => $filter['currency'],
            'order' => $order['currency'],
        ));

        $result = array();
        while ($currency = $currencies->fetch()) {
            $rates = MyCurrencyRateTable::getList(array(
                'select' => array('*'),
                'order' => array('DATE' => 'desc'),
                'filter' => array('=ID_CURRENCY' => $currency['ID']),
                'limit' => 1
            ));

            $rate = ($rates->fetchAll())[0];
            if (!empty($rate)) {
                array_push($result, array_merge($currency, array(
                    'NOMINAL' => $rate['NOMINAL'],
                    'RATE' => $rate['RATE'],
                    'DATE' => $rate['DATE'],
                )));
            }
        }

        foreach ($filter['rate'] as $field => $arFilter) {
            foreach ($arFilter as $fieldFilter) {
                $result = array_filter($result, self::build_filter($field, $fieldFilter));
            }
        }

        if (!empty($order['rate']['by']) && !empty($order['rate']['order'])) {
            usort($result, self::build_sorter($order['rate']['by'], $order['rate']['order']));
        }

        return $result;
    }

    /**
     * @param string $field - названия поля
     * @param string $sort - способ сортировки
     * @return \Closure
     */
    private static function build_sorter($field, $sort)
    {
        return function ($a, $b) use ($field, $sort) {
            if ((float)$a[$field] === (float)$b[$field]) {
                return 0;
            }
            if (strcasecmp($sort, 'desc')) {
                return ((float)$a[$field] < (float)$b[$field]) ? -1 : 1;
            } else {
                return ((float)$a[$field] > (float)$b[$field]) ? -1 : 1;
            }
        };
    }

    /**
     * @param string $field - название поля
     * @param array $filter - параметры фильтрации
     * @return \Closure
     */
    private static function build_filter($field, $filter)
    {
        return function ($arElement) use ($field, $filter) {
            $result = false;
            switch ($filter["filter"]) {
                case ">=":
                    $result = ((float)$arElement[$field] >= (float)$filter["value"]);
                    break;
                case "<=":
                    $result = ((float)$arElement[$field] <= (float)$filter["value"]);
                    break;
                case "=":
                    $result = ((float)$arElement[$field] === (float)$filter["value"]);
                    break;
            }
            return $result;
        };
    }
}
