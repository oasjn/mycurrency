<?php

namespace MyCurrency;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class MyCurrencyTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'my_currency';
    }

    public static function getUfId()
    {
        return 'MYCURRENCY';
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true,
            )),
            new Entity\StringField('CHCODE', array(
                'required' => true,
                'validation' => array(__CLASS__, 'validateChCodeMyCurrency'),
            )),
            new Entity\IntegerField('CODE', array(
                'required' => true,
            )),
            new Entity\StringField('NAME', array(
                'required' => true,
            )),
            new Entity\ReferenceField(
                'MYCURRENCY_RATE',
                'MyCurrency\MyCurrencyRate',
                array('=this.ID' => 'ref.ID_CURRENCY'),
                array('join_type' => 'LEFT')
            )
        );
    }

    public static function validateChCodeMyCurrency()
    {
        return array(
            new Entity\Validator\Unique(Loc::getMessage("MYCURRENCY_ERROR_MESSAGE_UNIQUE")),
        );
    }
}
