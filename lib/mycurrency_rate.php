<?php

namespace MyCurrency;

use Bitrix\Main\Entity;
use Exception;

class MyCurrencyRateTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'my_currency_rate';
    }

    public static function getUfId()
    {
        return 'MYCURRENCY_RATE';
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true,
            )),
            new Entity\DateField('DATE', array(
                'required' => true,
                'default_value' => function () {
                    return new \Bitrix\Main\Type\Date();
                }
            )),
            new Entity\FloatField('RATE', array(
                'required' => true,
            )),
            new Entity\StringField('NOMINAL', array(
                'required' => true,
            )),
            new Entity\IntegerField('ID_CURRENCY', array(
                'required' => true,
            )),
            new Entity\ReferenceField(
                'MYCURRENCY',
                'MyCurrency\MyCurrency',
                array('=this.ID_CURRENCY' => 'ref.ID'),
                array('join_type' => 'LEFT')
            ),
        );
    }

    public static function add(array $data)
    {
        try {
            $result = parent::add($data);
        } catch (Exception $e) {
            $result = new Entity\AddResult();
            if (strpos($e->getMessage(), '(1062)') !== false) {
                $result->addError(new Entity\EntityError($e->getMessage(), 'rate_unique'));
            } else {
                $result->addError(new Entity\EntityError($e->getMessage()));
            }
        }
        return $result;
    }
}
