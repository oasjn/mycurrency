<?php

namespace MyCurrency;

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Config\Option;

Loc::loadMessages(__FILE__);

class CurrencyList
{
    const TABLE_ID = "rate_list";
    const MODULE_ID = "mycurrency";

    // объект списка CAdminList
    private $adminList;
    // объект CAdminResult
    private $result;

    public function __construct()
    {
        $oSort = new \CAdminSorting($this::TABLE_ID, "ID", "desc");
        $this->adminList = new \CAdminList($this::TABLE_ID, $oSort);
        $this->adminList->InitFilter(array());
    }

    public function getAdminList()
    {
        return $this->adminList;
    }

    /**
     * @param string $targetAction - выбор всех записей
     * @param string $typeAction - тип события
     */
    public function handleActions($targetAction, $typeAction)
    {
        $IDs = $this->adminList->GroupAction();

        if ($IDs) {
            if ($targetAction === 'selected') {
                $IDs = CurrencyRate::getIDAllCurrencies();
            }

            foreach ($IDs as $id) {
                switch ($typeAction) {
                    case "delete":
                        $error = $this->deleteAction((int)$id);

                        if (!empty($error)) {
                            $this->adminList->AddGroupError($error, $id);
                        }
                        break;
                }
            }
        }
    }

    /**
     * @param integer $id
     * @return string - текст ошибки
     */
    private function deleteAction($id)
    {
        return Currency::deleteCurrency($id);
    }

    /**
     * Формирование объекта списка
     * @param array $request - массив параметров
     * @param bool $addActions - требуется ли добавить какие-либо действия
     */
    public function formListResult($request, $addActions = true)
    {
        $this->initResult($request);

        $this->addNavigation();
        $this->addRows($addActions, $request);
        $this->addHeaders();
        $this->addFooter($this->result->SelectedRowsCount());
        $this->addGroupAction();

        $this->сheckListMode();
    }

    private function initResult($request)
    {
        $filter = CurrencyRate::formFilter($request);
        $order = CurrencyRate::formOrder($request);

        $this->result = new \CAdminResult(null, $this::TABLE_ID);
        $arCurrencies = CurrencyRate::getCurrenciesWithLastRate($filter, $order);
        $this->result->InitFromArray($arCurrencies);
    }

    private function addNavigation()
    {
        $this->result->NavStart(Option::get($this::MODULE_ID, "number_of_records"));
        $this->adminList->NavText($this->result->GetNavPrint(Loc::getMessage("MYCURRENCY_NAV_TEXT_CURRENCY")));
    }

    private function addHeaders()
    {
        $headersName = [
            "ID" => Loc::getMessage("MYCURRENCY_HEADER_LIST_ID"),
            "CODE" => Loc::getMessage("MYCURRENCY_HEADER_LIST_CODE"),
            "CHCODE" => Loc::getMessage("MYCURRENCY_HEADER_LIST_CHCODE"),
            "NOMINAL" => Loc::getMessage("MYCURRENCY_HEADER_LIST_NOMINAL"),
            "NAME" => Loc::getMessage("MYCURRENCY_HEADER_LIST_NAME"),
            "RATE" => Loc::getMessage("MYCURRENCY_HEADER_LIST_RATE"),
            "DATE" => Loc::getMessage("MYCURRENCY_HEADER_LIST_DATE"),
        ];

        $headers = array();
        foreach ($headersName as $key => $value) {
            if (Option::get($this::MODULE_ID, $key) === 'Y') {
                array_push($headers, array(
                    "id" => $key,
                    "content" => $value,
                    "sort" => $key,
                    "default" => true,
                ));
            }
        }

        $this->adminList->AddHeaders($headers);
    }

    private function addRows($addActions, $request)
    {
        while ($arRes = $this->result->NavNext(true)) {
            $row =& $this->adminList->AddRow($arRes['ID'], $arRes);

            $arActions = array();
            if ($addActions) {
                $arActions = $this->addActions($arRes['ID'], $request);
            }

            $row->AddActions($arActions);
        }
    }

    private function addActions($id, $request)
    {
        $delete = array(
            "ICON" => "delete",
            "TEXT" => Loc::getMessage("MYCURRENCY_ACTION_DELETE"),
            "ACTION" => "if(confirm('"
                . Loc::getMessage("MYCURRENCY_DELETE_CONFIRM")
                . "')) "
                . $this->adminList->ActionDoGroup(
                    $id,
                    "delete",
                    "set_filter={$request["set_filter"]}" . "&"
                    . "filter_chcode={$request["filter_chcode"]}" . "&"
                    . "filter_rate_from={$request["filter_rate_from"]}" . "&"
                    . "filter_rate_to={$request["filter_rate_to"]}" . "&"
                    . "order={$request["order"]}" . "$"
                    . "by={$request["by"]}" . "$"
                )
        );
        return array($delete);
    }

    private function addFooter($rowsCount)
    {
        $this->adminList->AddFooter(
            array(
                array(
                    "title" => Loc::getMessage("MYCURRENCY_FOOTER_LIST_SELECTED_TITLE"),
                    "value" => $rowsCount
                ),
                array(
                    "counter" => true,
                    "title" => Loc::getMessage("MYCURRENCY_FOOTER_LIST_CHECKED_TITLE"),
                    "value" => "0"
                ),
            )
        );
    }

    private function addGroupAction()
    {
        $this->adminList->AddGroupActionTable(array(
            "delete" => Loc::getMessage("MYCURRENCY_LIST_DELETE_TITLE"),
        ));
    }

    private function сheckListMode()
    {
        $this->adminList->CheckListMode();
    }
}
