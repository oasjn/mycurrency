<?php

namespace MyCurrency;

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Currency
{
    /**
     * @return array
     */
    public static function getAll()
    {
        $result = MyCurrencyTable::getList(array(
            'select' => array('*'),
        ));
        return $result->fetchAll();
    }

    /**
     * @param object $currency - объект параметров валюты
     * @return array
     */
    public static function addCurrency($currency)
    {
        global $DB;

        $DB->StartTransaction();

        $addCurrencyResult = MyCurrencyTable::add(array(
            "CODE" => $currency->Vcode,
            "CHCODE" => $currency->VchCode,
            "NAME" => trim($currency->Vname),
        ));

        if (!$addCurrencyResult->isSuccess()) {
            $DB->Rollback();
            return $addCurrencyResult->getErrorMessages();
        }

        $id = $addCurrencyResult->getId();

        $addRateResult = Rate::addRate($id, $currency);
        if (!empty($addRateResult)) {
            $DB->Rollback();
            return $addRateResult;
        }

        $DB->Commit();
        return array();
    }

    /**
     * @param integer $id
     * @return string
     */
    public static function deleteCurrency($id)
    {
        if (!MyCurrencyTable::delete($id)) {
            return Loc::getMessage("MYCURRENCY_DELETE_ERROR");
        }
        return "";
    }
}
