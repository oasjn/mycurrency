<?php

namespace MyCurrency;

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Rate
{
    /**
     * @param integer $id - id добавленной валюты
     * @param object $currency - объект параметров курса
     * @return array
     */
    public static function addRate($id, $currency)
    {
        $result = MyCurrencyRateTable::add(array(
            "ID_CURRENCY" => $id,
            "RATE" => $currency->Vcurs,
            "NOMINAL" => $currency->Vnom,
            "DATE" => new \Bitrix\Main\Type\Date($currency->Vdate),
        ));

        if (!$result->isSuccess()) {
            $errors = $result->getErrors();

            $messages = array();
            foreach ($errors as $error) {
                if ($error->getCode() !== 'rate_unique') {
                    array_push($messages, $error->getMessage());
                }
            }
            return $messages;
        }
        return array();
    }
}
